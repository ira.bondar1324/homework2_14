"use strict";

const changeThem = document.querySelector(".change_them");
const secondChangeThem = document.querySelector(".second_change_them");
const header = document.querySelector(".header");
const main = document.querySelector(".main");
const ourClients = document.querySelector(".our_clients");


if(localStorage.getItem("activeBtn")==="changeThem"){
    changeThem.style.display ="block";
    secondChangeThem.style.display ="none";
} else if(localStorage.getItem("activeBtn")==="secondChangeThem"){
    secondChangeThem.style.display ="block";
    changeThem.style.display ="none";
}

header.style.backgroundColor = localStorage.getItem("bodyColor");
main.style.backgroundColor = localStorage.getItem("bodyColor");
ourClients.style.background= localStorage.getItem("ourClientsColor");

changeThem.addEventListener("click",()=>{
    localStorage.setItem("activeBtn","secondChangeThem");
    localStorage.setItem("bodyColor","#008B8B");
    localStorage.setItem("ourClientsColor","#2F4F4F");
    secondChangeThem.style.display ="block";
    changeThem.style.display ="none";
    header.style.backgroundColor = localStorage.getItem("bodyColor");
    main.style.backgroundColor = localStorage.getItem("bodyColor");
    ourClients.style.background= localStorage.getItem("ourClientsColor");
});

secondChangeThem.addEventListener("click",()=>{
    localStorage.setItem("activeBtn","changeThem");
    localStorage.setItem("bodyColor","#171717");
    localStorage.setItem("ourClientsColor","#0E0E0E");
    secondChangeThem.style.display ="none";
    changeThem.style.display ="block";
    header.style.backgroundColor = localStorage.getItem("bodyColor");
    main.style.backgroundColor = localStorage.getItem("bodyColor");
    ourClients.style.background= localStorage.getItem("ourClientsColor");
})